<?php

class Telegram_Order_Sender {
    private static $order_fields_map;

    public static function init(){
        add_action('woocommerce_order_status_changed', function($post_ID, $status_transition_from, $status_transition_to, $that){
			if(
				'processing' === $status_transition_to
			){
				self::onCreateProduct($post_ID);
			}
        }, 10, 4);
    }

    public static function get_order_items_info($items){
        foreach ($items as &$item){
            $item_attributes = [];
            foreach ($item -> get_meta_data() as &$attribute){
                $attr_key = $attribute->get_data()['key'];
                $attr_value = $attribute->get_data()['value'];
                $term_name = get_term_by('slug', $attr_value, $attr_key )->name;
                $item_attributes[] = $term_name;
            }
            $_item = $item->get_name();
            $_item .= '|'. implode( '|', $item_attributes) . '|';
            $_item .= "( {$item->get_quantity()}шт )";

            $item = $_item;
        }

        $message = implode( PHP_EOL , $items);
        return $message;
    }

    public static function get_total_price_block($items, $fee, $delivery){
        $price_block = '';
        $total_price = 0;
        foreach ($items as &$item){
            $single_price = $item->get_subtotal()/ $item->get_quantity();
            $total_price += $item->get_subtotal();
            $item = $item->get_name()."( {$single_price} грн )x".$item->get_quantity() .' = '. $item->get_subtotal() . 'грн';
        }

        if(key_exists('fee_package', $fee)){
            $fee = $fee['fee_package'];
            $total_price += $fee->get_total();
            $items [] = $fee->get_name() . ' = '. $fee->get_total() . 'грн';
        }elseif( key_exists('meta_package_name', $fee) ){
            $fee = $fee['meta_package_name'];
            foreach (get_field('boxes','options') as $box){
                $box_name = $box['name'];
                if(strpos($fee, $box_name) !== false){
                    $total_price += $box['price'];
                    $items [] = $box['name'] . ' = '. $box['price'] . 'грн';
                }
            }
        }

        if($delivery){
            $items []= "Доставка: {$delivery}грн";
            $total_price += $delivery;
        }

        $price_block .= implode(',' . PHP_EOL ,$items);
        $price_block .= PHP_EOL . "Загальна сума: {$total_price}грн";
        return $price_block ;
    }

    public static function get_full_address($address_array){
        $output = [];
        $output [] = $address_array['state'] ? $address_array['state'] . ' обл.' : '';
        $output [] = $address_array['city'] ? : '';
        $output [] = $address_array['address_1'] ? : '';

        return implode(', ', $output);
    }

    public static function formMessageByPattern($pattern, $wp_post_id){
        $order = new WC_Order($wp_post_id);

        $admin_order_link = get_admin_url()."post.php?post={$wp_post_id}&action=edit";

        if( false ){
            $package = ['fee_package' => $order->get_items('fee')[0]];
        }else{
            $package = ['meta_package_name' => ($order->get_meta('_onedeal_box_field') ? : '') ];
        }

        self::$order_fields_map = [
            'посилання' => '<a href="'.$admin_order_link.'">посилання на замовлення</a>',
            'ссылка' => '<a href="'.$admin_order_link.'">посилання на замовлення</a>',
            'номер заявки' => "<strong>{$wp_post_id}</strong>",
            'дата' => $order->order_date ? : '',
            'час' => $order->order_date ? : '',
            'ім\'я' => ( $order->get_billing_first_name() ?? '' ) .' '. ($order->get_billing_last_name() ?? ''),
            'імя' => ( $order->get_billing_first_name() ?? '' ) .' '. ($order->get_billing_last_name() ?? ''),
            'имя' => ( $order->get_billing_first_name() ?? '' ) .' '. ($order->get_billing_last_name() ?? ''),
            'телефон' => $order->get_billing_phone() ? : '',
            'email' => $order->get_billing_email() ? : '',
            'упаковка' => $order->get_meta('_onedeal_box_field') ? : '',
            'товари' => self::get_order_items_info($order->get_items()),
            'товары' => self::get_order_items_info($order->get_items()),
            'спосіб оплати' => $order->get_payment_method_title() ? : '',
            'доставка' => self::get_full_address($order->get_address('shipping')),
            'інкогніто' => $order->get_meta('_inkognito_field') ? : '',
            'чек' => self::get_total_price_block($order->get_items(), $package, $order->get_shipping_total()),
        ];


        $message = preg_replace_callback_array(
            [
                '/\[(.*?)\]/' => function ( $field ) {
                    $field = strtolower($field[1]);
                    return self::$order_fields_map[$field] ? : '';
                }
            ],
            $pattern
        );

        return $message;
    }

    public static function onCreateProduct($wp_post_id)
    {
        $bot_token = get_field('product_order_telegram_bot', 'options')['token'] ? : '' ;
        $groups = get_field('product_order_telegram_bot', 'options')['groups'] ?? [];
        if ( empty ( $groups ) ) {
            return;
        }

        $chats_ids = explode(PHP_EOL, $groups);
        $message_pattern = get_field('product_order_telegram_bot', 'options')['message_pattern'];

        $sender_bot = Telegram_Sender_Bot::getBot($bot_token, $chats_ids);

        if( !$sender_bot ){
            return "THE BOT WITH TOKEN {$bot_token} hasn't been found!";
        }


        $message = self::formMessageByPattern($message_pattern, $wp_post_id);

        $opts = [
            'text' => $message,
            'parse_mode' => 'HTML',
            'disable_notification' => get_field('product_order_telegram_bot', 'options')['disable_sound'] ? true : false
        ];
        $sender_bot->sendToAllChats($opts);

    }

    public static function log($message){
        $dir = wp_upload_dir()['path'];
        $date = date("y:m:d h:i:s");
        $handler = fopen($dir. '/tg_orders.log', "a+");
        fwrite($handler, PHP_EOL . "[{$date}]" . $message);
    }

}

Telegram_Order_Sender::init();

