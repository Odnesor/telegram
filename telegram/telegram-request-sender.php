<?php

class Telegram_Request_Sender{
    private static $order_fields_map;
    private static $sender_bot;
    private static $message_pattern;

    const STATUS_FIELD_VALUE_MAP = [
        'contact' => 'Зв\'язались з клієнтом',
        'book' => 'Записали клієнта',
        'changedate' => 'Перенесли на іншу дату',
        'paid' => 'Розрахувались з партнером'
    ];

    public static function init(){
        add_action('admin_post_activationorder_telegram_webhook', [ __CLASS__ , 'activationorder_webhook_handler']);
        add_action('admin_post_nopriv_activationorder_telegram_webhook',[ __CLASS__  , 'activationorder_webhook_handler']);
        add_action('acf/init', [ __CLASS__ , 'acf_add_local_field_group']);
    }

    public static function formMessageByPattern($pattern, $wp_post_id){
        $client_info = get_field('client_info',$wp_post_id)[0];

        self::$order_fields_map = [
            'дата заявки' => get_field('order_date',$wp_post_id),
            'iм\'я' => $client_info['name'] ? : '',
            'iмя' => $client_info['name'] ? : '',
            'имя' => $client_info['name'] ? : '',
            'номер телефону' => $client_info['phone'] ? : '',
            'номер телефона' => $client_info['phone'] ? : '',
            'пошта' => $client_info['email'] ? : '',
            'почта' => $client_info['email'] ? : '',
            'назва продукту' => $client_info['product_name'] ? : '',
            'назва продукта' => $client_info['product_name'] ? : '',
            'номер сертифікату' => $client_info['cert_number'] ? : '',
            'номер сертифіката' => $client_info['cert_number'] ? : '',
            'час' => $client_info['preferred_time'] ? : '',
            'дата' => $client_info['preferred_day'] ? : '',
            'повідомлення' => $client_info['description'] ? : '',

        ];

        $message = "ID Активації на заявку: ".'#id'. $wp_post_id;
        $message .= PHP_EOL;
        $message .= preg_replace_callback_array(
            [
                '/\[(.*?)\]/' => function ( $field ) {
                    $field = $field[1];
                    return self::$order_fields_map[$field];
                }
            ],
            $pattern
        );

        return $message;
    }

    public static function assignActivationSenderBot($_bot_token = '', $_chat_ids = ''){
        $bot_token = $_bot_token ? : get_field('activation_order_telegram_bot', 'options')['token'] ? : '' ;
        $chats_ids = $_chat_ids ? : explode(PHP_EOL, get_field('activation_order_telegram_bot', 'options')['groups'] ? : '');

        self::$message_pattern = get_field('activation_order_telegram_bot', 'options')['message_pattern'];

        return Telegram_Sender_Bot::getBot($bot_token, $chats_ids);
    }

    public static function onCreateActivationRequest($wp_post_id)
    {
        $sender_bot = self::assignActivationSenderBot();

        $sender_bot->checkWebHook();

        $message = self::formMessageByPattern(self::$message_pattern, $wp_post_id);
        $keyboard = json_encode(self::status_buttons_register_args($wp_post_id));


        $opts = [
            'text' => $message,
            'entities' => ['hashtag'],
            'disable_notification' => get_field('activation_order_telegram_bot', 'options')['disable_sound'] ? true : false,
            'reply_markup' => $keyboard
        ];
        $sender_bot->sendToAllChats($opts);
    }

    public static function status_buttons_register_args($wp_post_id){
        $wp_status = get_field('client_status', $wp_post_id) ? : '';
        $keyboard = [
            'inline_keyboard' => [
                [
                    [
                        'text' => (self::STATUS_FIELD_VALUE_MAP['contact'] === $wp_status ? '✅' : '') . 'Зв’язалась з партнером',
                        'callback_data' => json_encode(["wp_id" => $wp_post_id, "activation_action" => "contact"])
                    ],
                ],
                [
                    [
                        'text' => (self::STATUS_FIELD_VALUE_MAP['book'] === $wp_status ? '✅' : '') . 'Записали клієнта',
                        'callback_data' => json_encode(["wp_id" => $wp_post_id, "activation_action" => "book"])
                    ],
                ],
                [
                    [
                        'text' => (self::STATUS_FIELD_VALUE_MAP['changedate'] === $wp_status ? '✅' : '') . 'Перенесли на іншу дату',
                        'callback_data' => json_encode(["wp_id" => $wp_post_id, "activation_action" => "changedate"])
                    ],
                ],
                [

                    [
                        'text' => (self::STATUS_FIELD_VALUE_MAP['paid'] === $wp_status ? '✅' : '') . 'Розрахувались з партнером',
                        'callback_data' => json_encode(["wp_id" => $wp_post_id, "activation_action" => "paid"])
                    ],
                ]
            ]
        ];

        return $keyboard;
    }

    public static function activationorder_webhook_handler(){
        $sender_bot = self::assignActivationSenderBot();

        $webhook_post = json_decode(file_get_contents("php://input"), true);
        $activation_action_data = json_decode($webhook_post['callback_query']['data'], true);

        $message_id = $webhook_post['callback_query']['message']['message_id'];
        $chat_id = $webhook_post['callback_query']['message']['chat']['id'];
        $wp_post_id = $activation_action_data['wp_id'];
        $activation_new_status = $activation_action_data['activation_action'];

        $wp_post_update =[
            'ID' => $wp_post_id,
            'meta_input' => [
                'client_status' =>  self::STATUS_FIELD_VALUE_MAP[$activation_new_status],
                'tg_message_id' => $message_id,
                'chat_id' => $chat_id
            ]
        ] ;


        wp_update_post($wp_post_update, false, false);

        $message = self::formMessageByPattern(self::$message_pattern, $wp_post_id);
        $keyboard = json_encode(self::status_buttons_register_args($wp_post_id));

        $message_update_opts = [
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'text' => $message,
            'reply_markup' => $keyboard
        ];

        $sender_bot->sendToAllChats($message_update_opts);

        echo true;
    }

    public static function acf_add_local_field_group(){
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_5f96fff6b8b94',
                'title' => 'Заявки на активацію',
                'fields' => array(
                    array(
                        'key' => 'field_5f9700cb80ecb',
                        'label' => 'Дата заявки',
                        'name' => 'order_date',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'wpml_cf_preferences' => 2,
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5f97006e404e0',
                        'label' => 'Информація про клієнта',
                        'name' => 'client_info',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'wpml_cf_preferences' => 3,
                        'collapsed' => '',
                        'min' => 1,
                        'max' => 1,
                        'layout' => 'block',
                        'button_label' => 'Додати заявку на активацію',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5f970085404e1',
                                'label' => 'Имя',
                                'name' => 'name',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5f9700a7404e4',
                                'label' => 'Номер телефона',
                                'name' => 'phone',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                                'wpml_cf_preferences' => 1,
                            ),
                            array(
                                'key' => 'field_5f9700a1404e3',
                                'label' => 'Пошта',
                                'name' => 'email',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5fc7d8dc6d21b',
                                'label' => 'Назва продукту',
                                'name' => 'product_name',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_60227c1beefdd',
                                'label' => 'Номер сертифіката',
                                'name' => 'cert_number',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5fc7dd4081a7f',
                                'label' => 'Час',
                                'name' => 'preferred_time',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5fc7dd3581a7e',
                                'label' => 'Дата',
                                'name' => 'preferred_day',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_5fc9ef0229425',
                                'label' => 'Повідомлення',
                                'name' => 'description',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_611bc18590904',
                        'label' => 'Статус клієнта',
                        'name' => 'client_status',
                        'type' => 'radio',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_611ceb00b0c6e',
                                    'operator' => '!=empty',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'wpml_cf_preferences' => 0,
                        'choices' => array(
                            'Зв\'язались з клієнтом' => 'Зв\'язались з клієнтом',
                            'Записали клієнта' => 'Записали клієнта',
                            'Перенесли на іншу дату' => 'Перенесли на іншу дату',
                            'Розрахувались з партнером' => 'Розрахувались з партнером',
                        ),
                        'allow_null' => 1,
                        'other_choice' => 0,
                        'default_value' => '',
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                        'save_other_choice' => 0,
                    ),
                    array(
                        'key' => 'field_611ceb00b0c6e',
                        'label' => 'tg_message_id',
                        'name' => 'tg_message_id',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'hidden',
                            'id' => '',
                        ),
                        'wpml_cf_preferences' => 0,
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_611ced5541873',
                        'label' => 'chat_id',
                        'name' => 'chat_id',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'hidden',
                            'id' => '',
                        ),
                        'wpml_cf_preferences' => 0,
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_611cfcfc4cd91',
                        'label' => 'is_update_from_tg',
                        'name' => 'is_update_from_tg',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'hidden',
                            'id' => '',
                        ),
                        'wpml_cf_preferences' => 0,
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'activation_orders',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'acf_after_title',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => array(
                    0 => 'permalink',
                    1 => 'the_content',
                    2 => 'excerpt',
                    3 => 'discussion',
                    4 => 'comments',
                    5 => 'revisions',
                    6 => 'slug',
                    7 => 'author',
                    8 => 'format',
                    9 => 'page_attributes',
                    10 => 'featured_image',
                    11 => 'categories',
                    12 => 'tags',
                    13 => 'send-trackbacks',
                ),
                'active' => true,
                'description' => '',
            ));

        endif;
    }
}

Telegram_Request_Sender::init();



