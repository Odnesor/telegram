<?php

class Telegram_Options{
    public static function init(){
        add_action('acf/init', [__CLASS__,'acf_add_options_page']);
        add_action('acf/init', [__CLASS__,'acf_add_local_field_group']);
    }

    public static function acf_add_options_page() {
        if( function_exists('acf_add_options_page') ) {

            acf_add_options_page(array(
                'page_title' 	=> 'Налаштування telegram-бота',
                'menu_title'	=> 'Налаштування telegram-бота',
                'menu_slug' => 'telegram_options',
            ));
        }
    }

    public static function acf_add_local_field_group(){
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_60f1657330804',
                'title' => 'Telegram bot options',
                'fields' => array(
                    array(
                        'key' => 'field_6107a47e375b6',
                        'label' => 'Бот для заявок на активацію',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'wpml_cf_preferences' => 0,
                    ),
                    array(
                        'key' => 'field_60f166557448e',
                        'label' => '',
                        'name' => 'activation_order_telegram_bot',
                        'type' => 'group',
                        'instructions' => 'Настройки в админке для разных языков нe зависимы друг от друга. Если хотите поменять настройки отправки сообщений по telegram для обеих языков, изменения нужно вносить ДЛЯ ОБЕИХ ЯЗЫКОВ ОТДЕЛЬНО.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'layout' => 'block',
                        'wpml_cf_preferences' => 0,
                        'sub_fields' => array(
                            array(
                                'key' => 'field_60f166697448f',
                                'label' => 'токен telegram-бота',
                                'name' => 'token',
                                'type' => 'text',
                                'instructions' => 'Можна отримати після реєстрації бота через botFather</br>Приклад: 1710271497:AAFp9Ke-lFZ4CPUTGofFCQyB1eSrHckcjVs',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_60f1667e74490',
                                'label' => 'групи для відправки',
                                'name' => 'groups',
                                'type' => 'textarea',
                                'instructions' => 'id чатів для відправки (один id на рядок)',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => 4,
                                'new_lines' => '',
                                'wpml_cf_preferences' => 0,
                            ),
                            array(
                                'key' => 'field_60f16ebd74491',
                                'label' => 'Шаблон для повідомлення',
                                'name' => 'message_pattern',
                                'type' => 'textarea',
                                'instructions' => 'Введіть шаблон для формування повідомлення, обгортаючи потрібні значення у квадратні дужки 
<br/>значення:
<br/>[дата заявки]
<br/>[iм\'я]
<br/>[номер телефону]
<br/>[пошта]
<br/>[назва продукту]
<br/>[номер сертифікату]
<br/>[час]
<br/>[дата]
<br/>[повідомлення]',
                                'required' => 1,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'new_lines' => '',
                                'maxlength' => '',
                                'placeholder' => '',
                                'rows' => '',
                            ),
                            array(
                                'key' => 'field_60f691cbb38ea',
                                'label' => 'Вимкнути звук?',
                                'name' => 'disable_sound',
                                'type' => 'true_false',
                                'instructions' => 'Обравши цю опцію ви будете отримувати сповіщення про заявки без звуку',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'message' => '',
                                'default_value' => 0,
                                'ui' => 1,
                                'wpml_cf_preferences' => 0,
                                'ui_on_text' => '',
                                'ui_off_text' => '',
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_6107a48b375b7',
                        'label' => 'Бот для замовлень',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'wpml_cf_preferences' => 0,
                    ),
                    array(
                        'key' => 'field_6107a56f8b39b',
                        'label' => '',
                        'name' => 'product_order_telegram_bot',
                        'type' => 'group',
                        'instructions' => 'Настройки в админке для разных языков нe зависимы друг от друга. Если хотите поменять настройки отправки сообщений по telegram для обеих языков, изменения нужно вносить ДЛЯ ОБЕИХ ЯЗЫКОВ ОТДЕЛЬНО.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'layout' => 'block',
                        'wpml_cf_preferences' => 0,
                        'sub_fields' => array(
                            array(
                                'key' => 'field_6107a5878b39c',
                                'label' => 'токен telegram-бота',
                                'name' => 'token',
                                'type' => 'text',
                                'instructions' => 'Можна отримати після реєстрації бота через botFather</br>Приклад: 1710271497:AAFp9Ke-lFZ4CPUTGofFCQyB1eSrHckcjVs',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_6107a58d8b39d',
                                'label' => 'групи для відправки',
                                'name' => 'groups',
                                'type' => 'textarea',
                                'instructions' => 'id чатів для відправки (один id на рядок)',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => 4,
                                'new_lines' => '',
                                'wpml_cf_preferences' => 0,
                            ),
                            array(
                                'key' => 'field_6107a5968b39e',
                                'label' => 'Шаблон для повідомлення',
                                'name' => 'message_pattern',
                                'type' => 'textarea',
                                'instructions' => 'Введіть шаблон для формування повідомлення, обгортаючи потрібні значення у квадратні дужки 
<br/>значення:
<br/>[посилання]
<br/>[номер заявки][дата][час]
<br/>[ім\'я]
<br/>[телефон][email]
<br/>[упаковка][товари]
<br/>[спосіб оплати][доставка]
<br/>[інкогніто]
<br/>[чек]',
                                'required' => 1,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'new_lines' => '',
                                'maxlength' => '',
                                'placeholder' => '',
                                'rows' => '',
                            ),
                            array(
                                'key' => 'field_6107a59c8b39f',
                                'label' => 'Вимкнути звук?',
                                'name' => 'disable_sound',
                                'type' => 'true_false',
                                'instructions' => 'Обравши цю опцію ви будете отримувати сповіщення про заявки без звуку',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'message' => '',
                                'default_value' => 0,
                                'ui' => 1,
                                'wpml_cf_preferences' => 0,
                                'ui_on_text' => '',
                                'ui_off_text' => '',
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_613f4f92d4e9b',
                        'label' => 'Бот для заявок на подарунок в один клік',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                        'wpml_cf_preferences' => 0,
                    ),
                    array(
                        'key' => 'field_613f4ffbd4e9c',
                        'label' => '',
                        'name' => 'fastbuy_telegram_bot',
                        'type' => 'group',
                        'instructions' => 'Настройки в админке для разных языков нe зависимы друг от друга. Если хотите поменять настройки отправки сообщений по telegram для обеих языков, изменения нужно вносить ДЛЯ ОБЕИХ ЯЗЫКОВ ОТДЕЛЬНО.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'layout' => 'block',
                        'wpml_cf_preferences' => 0,
                        'sub_fields' => array(
                            array(
                                'key' => 'field_613f5014d4e9d',
                                'label' => 'токен telegram-бота',
                                'name' => 'token',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_613f5036d4e9e',
                                'label' => 'групи для відправки',
                                'name' => 'groups',
                                'type' => 'textarea',
                                'instructions' => 'id чатів для відправки (один id на рядок)',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'wpml_cf_preferences' => 0,
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_613f5042d4e9f',
                                'label' => 'Шаблон для повідомлення',
                                'name' => 'message_pattern',
                                'type' => 'textarea',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'maxlength' => '',
                                'rows' => '',
                                'new_lines' => '',
                                'wpml_cf_preferences' => 0,
                            ),
                            array(
                                'key' => 'field_613f5071d4ea0',
                                'label' => 'Вимкнути звук?',
                                'name' => 'disable_sound',
                                'type' => 'true_false',
                                'instructions' => 'Обравши цю опцію ви будете отримувати сповіщення про заявки без звуку',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'message' => '',
                                'default_value' => 0,
                                'ui' => 1,
                                'ui_on_text' => '',
                                'ui_off_text' => '',
                                'wpml_cf_preferences' => 0,
                            ),
                        ),
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'telegram_options',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

        endif;
    }
}

Telegram_Options::init();
