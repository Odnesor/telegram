<?php

class Telegram_Fastbuy_Sender
{
    private static $fastbuy_fields_map;
    private static $sender_bot;
    private static $message_pattern;

    public static function init()
    {
//        add_action('admin_post_activationorder_telegram_webhook', [__CLASS__, 'activationorder_webhook_handler']);
//        add_action('admin_post_nopriv_activationorder_telegram_webhook', [__CLASS__, 'activationorder_webhook_handler']);
//        add_action('acf/init', [__CLASS__, 'acf_add_local_field_group']);
    }

    public static function formMessageByPattern($pattern, $wp_post_id)
    {
        $client_info = get_field('client_info_fast', $wp_post_id)[0];
        
        self::$fastbuy_fields_map = [
            'дата' => get_field('order_date_fast', $wp_post_id),
            'дата заявки' => get_field('order_date_fast', $wp_post_id),
            'iм\'я' => $client_info['name'] ?: '',
            'iмя' => $client_info['name'] ?: '',
            'имя' => $client_info['name'] ?: '',
            'номер' => $client_info['phone'] ?: '',
            'номер телефону' => $client_info['phone'] ?: '',
            'номер телефона' => $client_info['phone'] ?: '',
            'продукт' => $client_info['product_name'] ?: '',
            'назва продукту' => $client_info['product_name'] ?: '',
            'назва продукта' => $client_info['product_name'] ?: '',

        ];


        $message = preg_replace_callback_array(
            [
                '/\[(.*?)\]/' => function ($field) {
                    $field = $field[1];
                    return self::$fastbuy_fields_map[$field];
                }
            ],
            $pattern
        );

        return $message;
    }

    public static function assignFastbuySenderBot($_bot_token = '', $_chat_ids = '')
    {
        $bot_token = $_bot_token ?: get_field('fastbuy_telegram_bot', 'options')['token'] ?: '';
        $chats_ids = $_chat_ids ?: explode(PHP_EOL, get_field('fastbuy_telegram_bot', 'options')['groups'] ?: '');

        self::$message_pattern = get_field('fastbuy_telegram_bot', 'options')['message_pattern'];

        return Telegram_Sender_Bot::getBot($bot_token, $chats_ids);
    }

    public static function onCreateFastbuyRequest($wp_post_id)
    {
        $sender_bot = self::assignFastbuySenderBot();

        $message = self::formMessageByPattern(self::$message_pattern, $wp_post_id);

        $opts = [
            'text' => $message,
            'parse_mode' => 'HTML',
            'disable_notification' => get_field('fastbuy_telegram_bot', 'options')['disable_sound'] ? true : false,
        ];
        $sender_bot->sendToAllChats($opts);
    }

    public static function acf_add_local_field_group()
    {

    }
}




