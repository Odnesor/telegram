<?php

class Telegram_Sender_Bot{
    private $bot_url;
    private $chats_ids;

    public static function getBot($bot_token = '', $chats_ids = []){
        if( !$bot_token || empty($chats_ids)){
            return false;
        }
        $bot_url = 'https://api.telegram.org/bot' . $bot_token;

        if( !json_decode(file_get_contents($bot_url . '/getMe'), true)['ok'] ){
            return false;
        }

        return new self($bot_url, $chats_ids);
    }

    public function __construct($bot_url, $chats_ids){
            $this->bot_url = $bot_url;
            $this->chats_ids = $chats_ids;
    }

    public function sendToAllChats($opts){
        $response = [];
        foreach($this->chats_ids as $chat_id){
            $opts['chat_id'] = $chat_id;
            $response [] = $this->callSendMessage($opts);
        }

        return $response;
    }

    public function callSendMessage( $opts ): string
    {
        $default_method = '/sendMessage';
        try {
            if( key_exists('message_id', $opts)){
                $method = '/editMessageText';
            }else{
                $method = '/sendMessage';
            }
            $response = $this->send($this->bot_url . $method, $opts );

            if(empty($response)){
                throw new Exception("empty response!");
            }

            if($response['ok']){
                $verification_success = $opts['chat_id'] . ':  success';
            }else{
                throw new Exception($opts['chat_id'] . ':  bot-ok false!');
            }

        }catch (Exception $e){
            $verification_success = $e->getMessage();
        }

        return $verification_success;
    }

    public function checkWebHook($_webhook_handler = ''){
        $bot_hook_info = json_decode($this->infoWebhook(), true);
        $bot_webhook_handler = $bot_hook_info['result']['url'];
        $current_webhook_handler = $_webhook_handler ? : ADMIN_POST . '?action=activationorder_telegram_webhook';

        if($bot_webhook_handler === $current_webhook_handler){
            return 'Current bot webhook is already assigned to project handler!';
        }

        $params = [
            'url' => $current_webhook_handler,
            'allowed_updates' => json_encode(['callback_query']),
            'max_connections' => 1,
            'drop_pending_updates' => true
        ];
        $response = $this->send($this->bot_url.'/setWebhook', $params);
        return $response;
    }

    public function deleteWebhooks(){
        $response = $this->send($this->bot_url.'/deleteWebhook', []);
        return $response;
    }

    public function infoWebhook(){
        return file_get_contents($this->bot_url.'/getWebhookInfo');
    }

    public function getUpdates(){
        $url = "{$this->bot_url}/getUpdates";
        $response = file_get_contents($url);
        return $response;
    }

    private function send( string $endpoint, array $payload ): array
    {
        $curl = curl_init();
        if ( ! $curl ) {
            throw new Exception('CURL INIT FAILURE!');
        }

        $curl_setup = [
            CURLOPT_URL => $endpoint,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $payload,
        ];

        if ( ! curl_setopt_array( $curl, $curl_setup ) ) {
            throw new Exception('CURL SETUP FAILURE!');
        }

        $response = json_decode(curl_exec( $curl ), true);
        if ( ! $response ) {
            throw new Exception('RETURNTRANSFER FAILURE');
        }

        curl_close( $curl );

        return $response;
    }
}

